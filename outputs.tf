output "cluster_id" {
  name = module.eks.cluster_id
}

output "cluster_enpoint" {
  name = module.eks.cluster_enpoint 
}

output "cluster_securty_group_id" {
  name = module.eks.cluster_securty_group_id
}

output "kubectl_config" {
  name = module.eks.kubectl_config
}

output "config_map_aws_auth" {
  name = module.eks.config_map_aws_auth
}

output "region" {
  name = var.region
}

output "cluster_name" {
  name = local.cluster_name
}