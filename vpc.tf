module "vpc" {
    source = "terraform-aws-modules/vpc/aws"
    version = "3.2.0"

    name = "wom-vpc"
    cidr = "10.0.0.0/16"
    azs = data.aws.availability_zone.availability.name
    private_subnet = ["10.0.1.0/24","10.0.2.0/24"]
    public_subnet =  ["10.0.3.0/24","10.0.4.0/24"]
    enable_nat_gateway = true
    single_nat_gateway = true



    tags = {
        "kubernetes.io/cluster/${local.cluster.name}" = "shared"
    }

    punlic_subnet_tags = {
        "kubernetes.io/cluster/${local.cluster.name}" = "shared"
        "kubernetes.io/role/internal-elb" ="1"
    }

    private_subnet_tags = {
        "kubernetes.io/cluster/${local.cluster.name}" = "shared"
        "kubernetes.io/role/internal-elb" ="1"
    }
}
