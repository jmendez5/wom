provider "kubernetes" {
    host = data.aws_eks_cluster.cluster.endpoint
    token = data.aws_eks_cluster.cluster.token
    cluster_ca_certificate = base64code(data.eks_cluster.cluster.certificate)
}