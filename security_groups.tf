resource "aws_security_group" "worker_group_mgmt_one" {
  name        = "worker_group_mgmtone_one"
  vpc_id      = module.vpc.vpc_id
  
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"

    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "worker_group_mgmt_two" {
  name        = "worker_group_mgmtone_two"
  vpc_id      = module.vpc.vpc_id
  
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"

    cidr_blocks = ["192.168.0.0/0"]
  }
}
