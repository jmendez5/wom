module "eks" {
  source = "terraform-aws-modules/eks/aws"
  version = ""
  cluster_name = local.cluster_name
  cluster_version = "1.20"
  subnets = module.vpc.private_subnet

  vpc_id = module.vpc.vpc_id


}

worker_group =[
    {
    name = "grupo-1"
    instance_type = "t2.small"
    additional_security_group_ids = [aws_security_groups.worker_group_mgmt_one]
    asg_desired_capacity = 3
    },
]

data "aws_eks_cluster" "cluster"{
    name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster"{
    name = module.eks.cluster_id
}

