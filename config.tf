provider "aws" {
  profile = ""
  region  = "us-eats-1"
}

locals {
  cluster_name = "wom-${random_integer.suffix.result}" #nombre cluster
}

resource "random_integer" "suffix" {
    min = 1
    max = 10  
}